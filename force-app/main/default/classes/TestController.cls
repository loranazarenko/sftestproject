@isTest
public class TestController {
    @isTest
    public static void test_test()
    {
        PageReference pageRef = Page.StartQuiz;
        Test.setCurrentPage(pageRef);
        StartQuizController controller = new StartQuizController();
        Test__c test = new Test__c(Name = 'TEST1');
        insert test;
        System.assertEquals(null, test.QuestionCount__c, 'QuestionCount is not null');     
        Question__c question1 = new Question__c(Question__c = 'Test Question',Test__c = test.Id);
        insert question1; 
        test = [Select Id,QuestionCount__c From Test__c];
        controller.questions = [Select Id,Question__c From Question__c Where Test__c =:test.Id];
        controller.answerByQuestion.put(controller.questions[0].Id, 'Ок'); 
        System.assertEquals(1, controller.questions.size());
        
        controller.startTest();
        System.assertEquals('Ок', controller.selectedVariant);
    }
}